FROM rocker/r-ubuntu:18.04
#FROM alpine

RUN apt-get update && apt-get install -y --no-install-recommends \
    sudo \
    libcurl4-gnutls-dev \
    libcairo2-dev \
    libxt-dev \
    libssl-dev \
    libudunits2-dev \
    libssh2-1-dev \
    libgdal-dev \
    libpoppler-cpp-dev \
    chromium-browser
#RUN apt-get install --assume-yes 
RUN apt-get clean


RUN mkdir -p /home/docker
WORKDIR /home/docker
COPY ./renv.lock ./renv.lock

RUN Rscript -e 'install.packages("renv")'
RUN Rscript -e 'renv::restore()'

COPY --chown=docker:docker . .
USER docker

#WORKDIR /home/janek/covmapp

# define the port number the container should expose
EXPOSE 4111

# run app
#CMD ["R", "--vanilla", "-s", "-e", "renv::restore()"]
CMD ["R", "-e", "shiny::runApp('app.R', host = '0.0.0.0', port = 4111)"]